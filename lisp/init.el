;;; -*- lexical-binding: t; -*-

(setq backup-by-copying t
      backup-directory-alist `(("." . ,(expand-file-name
                                        ".saves" user-emacs-directory)))
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control nil)

(require 'org)
(require 'org-element)
(setq org-element-cache-persistent nil)

(require 'ox)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)))

(setq org-export-with-sub-superscripts nil
      org-export-coding-system 'utf-8
      org-confirm-babel-evaluate nil)

(eval-when-compile (require 'rx))

(defvar lpru-set-toc-at-level-2-to-1 nil
  "Looks like non-nil won't be needed anymore but we keep it nevertheless.

It changes

#+TOC: headlines 2 local

to

#+TOC: headlines 1 local

at level 2 headings")

(defun lpru-remove-paragraph-titles-from-org ()
  (let ((limit 1))
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward (rx line-start (= 4 ?*) whitespace
                                    (group (zero-or-more whitespace)
                                           (one-or-more not-newline))
                                    line-end)
                                nil t)
        (delete-region (match-beginning 1) (match-end 1)))
      (goto-char (point-min))
      (when lpru-set-toc-at-level-2-to-1
        (while (re-search-forward (rx line-start (= 2 ?*) whitespace) nil t)
          (save-excursion
            (setq limit
                  (if (re-search-forward (rx line-start ?*) nil t)
                      (point) (point-max))))
          (save-excursion
            (re-search-forward (rx line-start (or "#+TOC:" "#+toc:")
                                   (zero-or-more whitespace)
                                   "headlines" " " anything)
                               limit t)
            (backward-delete-char 1)
            (insert ?1))
          (goto-char limit))))))

(defun file-directory-p+ (x) (when (file-directory-p x) x))

(require 'cl-seq)

(defmacro lpru-aand (&rest xs)
  (or (null xs)
      (cl-reduce (lambda (x a) `(let ((it ,a)) (and it ,x))) (reverse xs))))

(defun lpru-add-straight-subdir-to-load-path (package-name)
  (lpru-aand (file-directory-p+ (expand-file-name "straight/build"
                                                  user-emacs-directory))
             (car (cl-remove-if-not #'file-directory-p
                                    (directory-files it t package-name t)))
             (add-to-list 'load-path it)))

(defun lpru-add-latest-in-elpa-to-load-path (package-name)
  (lpru-aand (car (last (sort
                         (cl-remove-if-not #'file-directory-p
                                           (directory-files package-user-dir
                                                            t package-name t))
                         #'string-version-lessp)))
             (add-to-list 'load-path it)))

(defmacro lpru-make-o-o-binding (var)
  `(let ((gensym (make-symbol ,(symbol-name var))))
    `(,gensym ,(prog1 ,var (setq ,var gensym)))))
;; todo: replace this with use-package when Emacs 29 is popular enough
(defmacro lpru-require-carefully (package &optional file-base-name noerror)
  `(let (,(lpru-make-o-o-binding package))
     (or (require ,package nil t)
         (let (,(lpru-make-o-o-binding file-base-name))
           (or (progn (lpru-add-straight-subdir-to-load-path
                       ,file-base-name)
                      (require ,package nil t))
               (progn (require 'package)
                      (lpru-add-latest-in-elpa-to-load-path
                       ,file-base-name)
                      (require ,package nil ,noerror)))))))

(eval-when-compile (require 'subr-x))

(defun akater-org-id-assert-uniqueness-of-custom-ids ()
  (let (ids)
    (org-element-map (org-element-parse-buffer 'headline) 'headline
      (lambda (hl)
        (when-let* ((id (org-element-property :CUSTOM_ID hl)))
          (if (cl-member id ids :test #'string-equal)
              (error "Non-unique CUSTOM_ID %s at point %s"
                     id (org-element-property :begin hl))
            (push id ids))
          id)))))

(eval-when-compile (require 'cl-macs))

(defvar lpru-tildify-pattern "\\(?:[,:;(][ \t]*[a]\\|\\<[АВИКОСУЕавикосуе]\\)\\([ \t]+\\|[ \t]*\n[ \t]*\\)\\(?:\\w\\|[([{\\]\\|<[а-яА-Я]\\)")

(defun lpru-bylaws-make-ensure-style ()
  ;; this is basically (lpru-org-ensure-common-style 'build)
  ;; but we have not packaged lpru-org satisfactorily yet
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward (rx " " (or ?- ?–) " ") nil t)
      (replace-match " — "))
    (while (search-forward " – " nil t)
      (replace-match " — "))
    (setq-local tildify-pattern lpru-tildify-pattern)
    (tildify-buffer t)
    (goto-char (point-min))
    (while (search-forward " — " nil t)
      (replace-match " — "))
    (goto-char (point-min))
    (while (re-search-forward (rx (group (or " " " ")) "п. [") nil t)
      (replace-match (concat (match-string 1) "п. [")))
    (goto-char (point-min))
    (while (re-search-forward (rx (group digit)
                                  " "
                                  (group (or "дней"    "дня"
                                             "месяцев" "месяца"
                                             "часов"   "часа"
                                             "лет"     "года"))
                                  word-boundary)
                              nil t)
      (replace-match (concat (match-string 1) " " (match-string 2))))
    (goto-char (point-min))
    (while (search-forward "  " nil t)
      (replace-match " "))))

(defmacro lpru-aprog1 (first &rest body)
  "Like `prog1', but the result of evaluating FIRST is bound to `it'.

The variable `it' is available within BODY.

FIRST and BODY are otherwise as documented for `prog1'."
  (declare (indent 1))
  `(let ((it ,first)) ,@body it))

(require 'simple)
(require 'prog-mode)
(require 'files)

(cl-defun lpru-bylaws-make ( &key html pdf paragraph-titles (timestamp t)
                             remove-logfiles
                             &aux (build-timestamp
                                   (format-time-string "-%Y%m%d"))
                             (build-filename
                              (concat "bylaws" build-timestamp ".org"))
                             (build-name (file-name-base build-filename)))
  (defvar lpru-bylaws-make-pdf pdf)
  (defvar lpru-bylaws-make-html html)
  (ignore-errors (make-directory "build"))
  (copy-file "bylaws.org" (expand-file-name build-filename "build") t t t t)
  (let ((default-directory (expand-file-name "build")))
    (make-symbolic-link "../style" "style" t)
    (with-current-buffer (find-file-noselect build-filename)
      (akater-org-id-assert-uniqueness-of-custom-ids)
      (lpru-bylaws-make-ensure-style)
      (unless paragraph-titles (lpru-remove-paragraph-titles-from-org))
      (when html
        (message "")
        (message "Building %s.html" build-name)
        (let (built-file)
          (with-current-buffer (find-file-noselect
                                (setq built-file (org-html-export-to-html)))
            (message "Built %s" (file-name-nondirectory built-file))
            (message "Applying custom style manually...")
            (goto-char (point-min))
            (let ((style-expression-position 0))
              (replace-region-contents
               (progn
                 (re-search-forward (rx line-start "<style"))
                 (end-of-line)
                 (point))
               (progn
                 (re-search-forward (rx line-start "</style>"))
                 (beginning-of-line)
                 (point))
               (lambda () (string-join
                           (list ""
                                 (lpru-aprog1 " <!--/*--><![CDATA[/*><!--*/ "
                                   (setq style-expression-position
                                         (+ 2 (point) (length it))))
                                 " /*]]>*/-->"
                                 "")
                           "\n")))
              (goto-char style-expression-position)
              (insert-file-contents "style/adhoc.css")
              (previous-logical-line 2)
              ;; web-mode is needed to indent properly
              ;; we'll do without for now
              (prog-indent-sexp))
            (save-buffer)
            (message "Custom style applied")
            (copy-file built-file
                       (expand-file-name
                        (concat (concat "bylaws"
                                        (if timestamp build-timestamp ""))
                                ".html")
                        "..")
                       t))))
      (when pdf
        (message "")
        (message "Building %s.pdf" build-name)
        (unless (lpru-require-carefully 'tex "auctex" t)
          (warn "auctex not found, installing it from gnu now...")
          (require 'package)
          (setq package-enable-at-startup nil)
          (when (< emacs-major-version 27) (package-initialize))
          (setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")))
          (package-refresh-contents)
          (package-install 'auctex)
          (lpru-add-latest-to-load-path "auctex")
          (require 'tex))
        (let (built-file)
          (if (ignore-errors (let ((org-latex-remove-logfiles remove-logfiles))
                               (setq built-file (org-latex-export-to-pdf))))
              (progn
                (message "Built %s" (file-name-nondirectory built-file))
                (copy-file built-file
                           (expand-file-name
                            (concat (concat "bylaws"
                                            (if timestamp build-timestamp ""))
                                    ".pdf")
                            "..")
                           t))
            (message "%s"
                     (with-current-buffer (get-buffer "*Org PDF LaTeX Output*")
                       (buffer-string))))))
      (kill-buffer))))
