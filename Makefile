# makefile for lpru bylaws 2020.

# Author: Dima Akater

# Emacs invocation
EMACS_COMMAND   := emacs

# Use -q to have /usr/local/share/emacs/site-lisp and subdirs in load-path
EMACS		:= $(EMACS_COMMAND) -L lisp --batch --no-site-file

EMACS_INIT_EVAL := $(EMACS) --load init.el --eval

default:
	$(EMACS) --load init.el --load make.el

.PHONY: clean all

all:
	$(EMACS_INIT_EVAL) "(lpru-bylaws-make :html t :pdf t)"

html:
	$(EMACS_INIT_EVAL) "(lpru-bylaws-make :html t)"

pdf:
	$(EMACS_INIT_EVAL) "(lpru-bylaws-make :pdf t)"

# Remove build directory
clean:
	rm -rf build bylaws*.html bylaws*.pdf

PREFIX=/usr/local/
DESTDIR=${PREFIX}share/doc/lpru-bylaws
install:
	test -d ${DESTDIR} || mkdir ${DESTDIR}
	rm -f ${DESTDIR}*.html
	rm -f ${DESTDIR}*.pdf
uninstall:
	rm -vf ${DESTDIR}*.html
	rm -vf ${DESTDIR}*.pdf
